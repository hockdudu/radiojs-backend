module.exports = function events(req, res) {
  res.writeHead(200, {
    'Connection': 'keep-alive',
    'Cache-Control': 'no-cache',
    'Content-Type': 'text/event-stream'
  });

  const messageWriter = new MessageWriter(req, res);
  const eventListener = new EventListener(messageWriter);
  eventListener.bindListeners();
  eventListener.bindKeepalive();
};

