module.exports = {
  friendlyName: 'Play',
  description: 'Plays the current radio',
  fn: async function () {
    await sails.hooks.mpc.stop();
    await sails.hooks.mpc.play();
  }
};
