module.exports = {
  friendlyName: 'Get all radios',
  description: 'Returns all radios as JSON',
  fn: async function (inputs, exits) {
    const radios = await Radio.find();
    return exits.success(radios);
  }
};
