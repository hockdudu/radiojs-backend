module.exports = {

  friendlyName: 'Current status',
  description: 'Gets the current status of the radio',

  fn: async function () {
    return {
      songTitle: await sails.hooks.mpc.getSongTitle(),
      volume: await sails.hooks.mpc.getVolume(),
      state: await sails.hooks.mpc.getState(),
      radio: await sails.hooks.mpc.getRadio(),
    };
  }
};
