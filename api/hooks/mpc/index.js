const MPC = require('mpc-js').MPC;

/**
 * mpc hook
 *
 * @description :: A hook definition.  Extends Sails by adding shadow routes, implicit actions, and/or initialization logic.
 * @docs        :: https://sailsjs.com/docs/concepts/extending-sails/hooks
 */

module.exports = function defineMpcHook(sails) {

  /**
   * @type MPC|EventEmitter
   */
  let mpc;

  return {

    /**
     * Runs when this Sails app loads/lifts.
     */
    initialize: function() {
      sails.log.info('Initializing custom hook (`mpc`)');
      initializeMpc();
    },

    play: async function() {
      return mpc.playback.play();
    },

    pause: async function() {
      return mpc.playback.pause();
    },

    stop: async function() {
      return mpc.playback.stop();
    },

    changeRadio: async function(radio) {
      await mpc.currentPlaylist.clear();
      await mpc.currentPlaylist.add(radio.url);
      sails.emit('hook:mpc:radio', radio);
    },

    changeVolume: async function(volume) {
      await mpc.playbackOptions.setVolume(volume);
    },

    getSongTitle: async function() {
      return await getSongTitle();
    },

    getVolume: async function() {
      const volume = (await getStatus()).volume;
      return Number.isNaN(volume) ? 0 : volume;
    },

    getState: async function() {
      return (await getStatus()).state;
    },

    getRadio: async function() {
      return tryFindRadio();
    }
  };

  async function initializeMpc() {
    mpc = new MPC();
    try {
      await initializeMpcConnection();
      sails.log.info('Connected to mpc');
    } catch (e) {
      sails.log.error('Error connecting to mpc', e);
      sails.log.error('Retrying in 5 seconds');
      setTimeout(() => {
        initializeMpc();
      }, 5000);
    }

    await initializeRequiredConfiguration();
    initializeListeners();
  }

  function initializeMpcConnection() {
    return mpc.connectTCP(sails.config.custom.mpc.hostname, sails.config.custom.mpc.port);
  }

  function initializeRequiredConfiguration() {
    return mpc.playbackOptions.setRepeat(true);
  }

  function initializeListeners() {
    mpc.on('changed-playlist', notifyChangedPlaylist);
    mpc.on('changed-mixer', notifyChangedMixer);
    mpc.on('socket-end', () => {
      sails.log.error('Connection to mpc closed');
      initializeMpc();
    });
  }

  function notifyChangedPlaylist() {
    getSongTitle().then(songTitle => {
      sails.emit('hook:mpc:songName', songTitle);
    });

    tryFindRadio().then(radio => {
      sails.emit('hook:mpc:radio', radio);
    });
  }

  function notifyChangedMixer() {
    getStatus().then(status => {
      const volume = Number.isNaN(status.volume) ? 0 : status.volume;
      sails.emit('hook:mpc:volume', volume);
      sails.emit('hook:mpc:state', status.state);
    });
  }

  function getSongTitle() {
    return new Promise((resolve) => {
      mpc.status.status().then(status => {
        if (status.state === 'play' || status.state === 'pause') {
          mpc.status.currentSong().then(currentSong => {
            resolve(currentSong.title || currentSong.name || '');
          });
        } else {
          resolve('');
        }
      });
    });
  }

  function getStatus() {
    return mpc.status.status();
  }

  async function tryFindRadio() {
    const currentSong = await mpc.status.currentSong();

    if (typeof currentSong === 'undefined') {
      return null;
    }

    const currentRadio = await Radio.findOne({'url': currentSong.path});

    if (typeof currentRadio === 'undefined') {
      return null;
    }

    return currentRadio;
  }
};
