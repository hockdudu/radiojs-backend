const {default: PQueue} = require('p-queue');

class EventListener {

  /**
   * @param {MessageWriter} messageWriter
   */
  constructor(messageWriter) {
    this.queue = new PQueue({concurrency: 1});
    this.intervalId = 0;
    this.listenersMap = this._getListenersMap();
    this.messageWriter = messageWriter;
    this.messageWriter.onDisconnect = this._onDisconnect.bind(this);
  }

  bindListeners() {
    this.listenersMap.forEach((listener, eventName) => {
      sails.on(eventName, listener);
    });
  }

  bindKeepalive() {
    this.intervalId = setInterval(() => {
      this.queue.add(() => {
        this.messageWriter.sendPing();
      });
    }, sails.config.custom.sseKeepalive);

    // Sending it at the start lets the client know the server is working
    this.messageWriter.sendComment('open');
  }

  _getListenersMap() {
    const listenersMap = new Map();
    listenersMap.set('hook:mpc:songName', (songName) => this._eventSongName(songName));
    listenersMap.set('hook:mpc:volume', (volume) => this._eventVolume(volume));
    listenersMap.set('hook:mpc:state', (state) => this._eventState(state));
    listenersMap.set('hook:mpc:radio', (radio) => this._eventRadioChange(radio));
    listenersMap.set('hook:dataListener:create', (data) => this._eventDataListenerCreate(data));
    listenersMap.set('hook:dataListener:destroy', (data) => this._eventDataListenerDestroy(data));
    listenersMap.set('hook:dataListener:update', (data) => this._eventDataListenerUpdate(data));
    listenersMap.set('lower', () => this._eventLower());

    return listenersMap;
  }

  _eventSongName(songName) {
    this.queue.add(() => {
      this.messageWriter.writeMessage(songName, 'song_name');
    });
  }

  _eventVolume(volume) {
    this.queue.add(() => {
      this.messageWriter.writeMessage(volume.toString(), 'volume');
    });
  }

  _eventState(state) {
    this.queue.add(() => {
      this.messageWriter.writeMessage(state, 'state');
    });
  }

  _eventRadioChange(radio) {
    this.queue.add(() => {
      this.messageWriter.writeMessage(JSON.stringify(radio), 'radio');
    });
  }

  _eventDataListenerCreate(data) {
    this.queue.add(() => {
      this.messageWriter.writeMessage(JSON.stringify(data), 'create');
    });
  }

  _eventDataListenerDestroy(data) {
    this.queue.add(() => {
      this.messageWriter.writeMessage(JSON.stringify(data), 'delete');
    });
  }

  _eventDataListenerUpdate(data) {
    this.queue.add(() => {
      this.messageWriter.writeMessage(JSON.stringify(data), 'update');
    });
  }

  _eventLower() {
    this.messageWriter.endStream();
  }

  _onDisconnect() {
    this._unbindListeners();
    this._clearKeepalive();
  }

  _unbindListeners() {
    this.listenersMap.forEach((listener, eventName) => {
      sails.off(eventName, listener);
    });
  }

  _clearKeepalive() {
    clearInterval(this.intervalId);
  }
}

module.exports = EventListener;
